import datetime
from django.db import models
from django.utils import timezone

class Device_model(models.Model):
    model_name = models.CharField(max_length=100)

    def __str__(self):
        return self.model_name

class GUI_hosts(models.Model):
    GUI_fqdn = models.CharField(max_length=50)
    GUI_ip = models.CharField(max_length=15)
    GUI_notes = models.CharField(max_length=200)

    def __str__(self):
        return self.GUI_fqdn

class Device(models.Model):
    device_fqdn = models.CharField(max_length=50)
    device_ip = models.CharField(max_length=15)
    device_sn = models.CharField(max_length=15)
    device_mac = models.CharField(max_length=15)
    device_notes = models.CharField(max_length=200)
    device_added = models.DateTimeField('Date Device Added')
    device_model = models.ForeignKey(Device_model, on_delete=models.CASCADE)
    device_GUI = models.ForeignKey(GUI_hosts, on_delete=models.CASCADE)

    def __str__(self):
        return self.device_fqdn

    def date_added(self):
        return self.device_added >= timezone.now() - datetime.timedelta(days=1)
