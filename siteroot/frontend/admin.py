from django.contrib import admin
from .models import Device_model, GUI_hosts, Device

admin.site.register(Device_model)
admin.site.register(GUI_hosts)
admin.site.register(Device)
