from django.http import HttpResponse

from .models import Device

def index(request):
    latest_devices_list = Device.objects.order_by('-device_added')[:5]
    output = ' , '.join([q.device_fqdn for q in latest_devices_list])
    return HttpResponse(output)
